package com.jackpilcher.twittercompare;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.jackpilcher.twittercompare.models.Comparison;
import com.jackpilcher.twittercompare.models.Word;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.AppSession;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.GuestCallback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Search;
import com.twitter.sdk.android.core.services.SearchService;

import java.util.Arrays;
import java.util.List;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "bCA73epnnW2FRGBnsVVSvuCTK";
    private static final String TWITTER_SECRET = "2jiCxGDyGf4okEqHyBYvg2X76rNMCJsVhb89g45J2KVlvMNhvg";

    EditText wordOne;
    EditText wordTwo;

    ProgressBar progressBar;

    RecyclerView recyclerView;

    private List<EditText> mWords;
    private TwitterApiClient mTwitterApiClient;
    private SearchService mSearchService;
    private int mComplete = 0;

    private Comparison mComparison;
    private DatabaseReference mRef;
    private FirebaseRecyclerAdapter<Comparison, ComparisonHolder> mAdapter;
    private LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        // UI
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        progressBar = (ProgressBar) findViewById(R.id.progress);

        wordOne = (EditText) findViewById(R.id.word_one);
        wordTwo = (EditText) findViewById(R.id.word_two);

        mWords = Arrays.asList(wordOne, wordTwo);

        recyclerView = (RecyclerView) findViewById(R.id.comparison_list);

        // setup fabric and twitter
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));

        TwitterCore.getInstance().logInGuest(new Callback<AppSession>() {
            @Override
            public void success(Result<AppSession> result) {
                AppSession session = result.data;
                mTwitterApiClient = TwitterCore.getInstance().getApiClient(session);
                mSearchService = mTwitterApiClient.getSearchService();
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(MainActivity.this, "Failed to authenticate with twitter.", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void doTwitterSearch(final EditText editText) {

        mSearchService.tweets(editText.getText().toString(), null, "en", null, null, 100, null, null, null, null, new GuestCallback<>(new Callback<Search>() {
            @Override
            public void success(Result<Search> result) {
                int count = result.data.tweets.size();

                // display the results views
                ViewGroup parent = (ViewGroup) editText.getParent().getParent();
                ViewGroup resultsContainer = (ViewGroup) parent.findViewWithTag("results_container");
                TextView results = (TextView) resultsContainer.findViewWithTag("results");

                results.setText(Integer.toString(count));
                resultsContainer.setVisibility(View.VISIBLE);

                Word word = new Word(editText.getText().toString(), count);
                // async api call so the words might not be in order, but for this it doesn't matter so much
                mComparison.addWord(word);

                mComplete = mComplete + 1;
                if (mComplete == mWords.size()) {
                    progressBar.setVisibility(View.INVISIBLE);
                    mComplete = 0;
                    saveComparison();
                }
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(MainActivity.this, "Whoops!", Toast.LENGTH_SHORT).show();
            }
        }));
    }

    private void saveComparison() {
        mRef.push().setValue(mComparison);
    }

    public void search(View view) {
        if (!validateInputs()) {
            Toast.makeText(MainActivity.this, "Please fix the form errors.", Toast.LENGTH_SHORT).show();
        } else {
            progressBar.setVisibility(View.VISIBLE);
            mComparison = new Comparison();
            hideKeyboard();
            for (EditText word : mWords) {
                doTwitterSearch(word);
            }
        }
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private boolean validateInputs() {
        boolean valid = true;

        for (EditText word : mWords) {
            if (TextUtils.isEmpty(word.getText())) {
                word.setError("Please enter a valid word.");
                valid = false;
            }
        }

        return valid;
    }

    public void reset(View view) {
        for (EditText word : mWords) {
            ViewGroup parent = (ViewGroup) word.getParent().getParent();
            ViewGroup resultsContainer = (ViewGroup) parent.findViewWithTag("results_container");
            resultsContainer.setVisibility(View.INVISIBLE);

            word.setText("");
        }
    }

    public void attachAdapter() {
        Query query = mRef.orderByKey().limitToLast(25);
        mAdapter = new FirebaseRecyclerAdapter<Comparison, ComparisonHolder>(Comparison.class, R.layout.list_item, ComparisonHolder.class, query) {
            @Override
            protected void populateViewHolder(ComparisonHolder viewHolder, Comparison comparison, int position) {
                int countOne = comparison.words.get(0).count;
                int countTwo = comparison.words.get(1).count;

                viewHolder.resultsOne.setText(Integer.toString(countOne));
                viewHolder.wordOne.setText(comparison.words.get(0).text);

                viewHolder.resultsTwo.setText(Integer.toString(countTwo));
                viewHolder.wordTwo.setText(comparison.words.get(1).text);

                if (countOne != countTwo) {
                    int accentColor = ContextCompat.getColor(MainActivity.this, R.color.colorAccent);
                    if (countOne > countTwo) {
                        viewHolder.wordOne.setTextColor(accentColor);
                    } else {
                        viewHolder.wordTwo.setTextColor(accentColor);
                    }
                }
            }
        };

        mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int start, int count) {
                mLayoutManager.smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount());
            }
        });

        recyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // firebase
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mRef = database.getReference("comparisons");

        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(mLayoutManager);
        attachAdapter();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAdapter != null) {
            mAdapter.cleanup();
        }
    }
}
