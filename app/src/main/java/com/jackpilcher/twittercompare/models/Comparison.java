package com.jackpilcher.twittercompare.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Me on 7/28/2016.
 */
@IgnoreExtraProperties
public class Comparison {

    public List<Word> words = new ArrayList<>();
    long date;


    public Comparison() {
        date = new Date().getTime();
    }


    @Exclude
    public void addWord(Word word) {
        words.add(word);
    }
}
