package com.jackpilcher.twittercompare.models;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Me on 7/28/2016.
 */
@IgnoreExtraProperties
public class Word {

    public String text;
    public int count;


    public Word () {

    }

    public Word (String text, int count) {
        this.text = text;
        this.count = count;
    }
}
