package com.jackpilcher.twittercompare;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Me on 7/28/2016.
 */
public class ComparisonHolder extends RecyclerView.ViewHolder {
    TextView wordOne;
    TextView wordTwo;

    TextView resultsOne;
    TextView resultsTwo;

    public ComparisonHolder(View itemView) {
        super(itemView);

        wordOne = (TextView) itemView.findViewById(R.id.word_one);
        wordTwo = (TextView) itemView.findViewById(R.id.word_two);

        resultsOne = (TextView) itemView.findViewById(R.id.results_one);
        resultsTwo = (TextView) itemView.findViewById(R.id.results_two);
    }
}
