# README #

Compares the popularity of words on twitter. Due to the way the twitter api behaves you're limited to 100 results without many query options. Comparing popular words will result in a result of 100 for both.

Uses Firebase as a database service. Users can see the last 25 comparisons from all users.

If complexity increases, or wanting to include more device we'd want to split the comparing of words and the listing of comparisons into fragments or partial views.